-- Test harness for gltf

local mat4 = require 'cpml.modules.mat4'
local gltf = require 'gltf'

local PlayerCamera = require 'playercamera'
local testFiles = require 'testfiles'
local debugUI = require 'debugUI'

local asset
local assetInstance
local renderer
local cam
local canvas
local jprof
function love.load(argv)
	local assetName = "RiggedSimple"
	local format = "loose"
	local assetPath = false
	for _, arg in ipairs(argv) do
		if assetName == true then
			assetName = arg
		elseif arg == "--profile" then
			_G.PROF_CAPTURE = true
			_G.PROF_REALTIME = false
		elseif arg == "--profile-realtime" then
			_G.PROF_CAPTURE = true
			_G.PROF_REALTIME = true
		elseif arg == "--binary-format" then
			format = "binary"
		elseif arg == "--embedded-format" then
			format = "embedded"
		elseif arg == "--builtin" then
			assetName = true
		else
			assetPath = arg
		end
	end

	jprof = require 'jprof'
	if PROF_REALTIME then
		jprof.connect()
	else
		jprof.enableThreadedWrite()
	end

	jprof.push("frame")
	debugUI:inject()
	if PROF_CAPTURE then
		require 'gltf-hooks'
	end
	jprof.push("love.load()")

	if not assetPath then
		assetPath = testFiles[assetName][format]
	end
	gltf.setLoadingStrategy('incremental')
	asset = gltf.newAsset(assetPath)

	renderer = gltf.newRenderer()

	cam = PlayerCamera.new()
	cam:grabInput()

	local w, h = love.graphics.getDimensions()
	canvas = love.graphics.newCanvas(w, h, {msaa=4})
	renderer:setCanvases(canvas, love.graphics.newCanvas(w, h, {format='depth32f'}))
	love.graphics.setBackgroundColor(0, 0, 0)

	local ratio = w / h
	local projection = mat4.from_perspective(45, ratio, .1, 1000)
	renderer:setProjectionMatrix(projection)

	jprof.pop("love.load()")
	jprof.pop("frame")
end

function love.quit()
	cam:stopInput()
	if PROF_CAPTURE and not PROF_REALTIME then
		print("writing profile.mpack")
		jprof.write("profile.mpack")
	end
end

function love.update(dt)
	jprof.push("frame")
	if not assetInstance then
		if not asset:isReady() then
			asset:continueLoading(0.001)
		else
			local sceneIdx = 1
			assetInstance = asset:newInstance(sceneIdx)
			assetInstance:playAllAnimations('loop')
		end
	else
		assetInstance:updateAllAnimations(dt)
	end

	cam:update(dt)

	local imgui = debugUI:get()
	local numAnimations = asset:getNumAnimations()
	if imgui and assetInstance and numAnimations > 0 then
		local title = string.format("Animator(%s)", assetInstance:getAsset().filename)
		if imgui.Begin(title) then
			if imgui.Button("Play All") then
				assetInstance:playAllAnimations()
			end
			imgui.SameLine()
			if imgui.Button("Stop All") then
				assetInstance:stopAllAnimations()
			end
			for i = 1, numAnimations do
				local name = asset:getAnimationName(i)
				local playHead = assetInstance:getAnimationPlayhead(i)
				name = ("%d: %s%s###%d"):format(i, name, playHead.state == "active" and " (active)" or "", i)
				imgui.SetNextTreeNodeOpen(true, "ImGuiCond_Appearing")
				if imgui.TreeNode(name) then
					imgui.LabelText("state", playHead.state)
					if imgui.Button("Play") then
						assetInstance:playAnimation(i)
					end
					imgui.SameLine()
					if imgui.Button("Stop") then
						assetInstance:stopAnimation(i)
					end
					if playHead.maxTime ~= 0 then
						local label = ("%.2f/%.2f"):format( playHead.time, playHead.maxTime)
						imgui.SliderFloat("time", playHead.time, 0, playHead.maxTime, label)
					end
					imgui.TreePop()
				end
			end
		end
		imgui.End()
	end
end

function love.draw()
	if assetInstance then
		renderer:setViewMatrix(cam:getTransform())
		renderer:addToDrawList(assetInstance)
		renderer:draw()

		love.graphics.draw(canvas)
	else
		love.graphics.printf("Loading", 0, 200, 800, 'center')
	end

	local warning = table.concat(asset.warnings, "\n")
	love.graphics.printf(warning, 0, 0, 800)

	local right_hud = string.format("fps: %d\n%s", love.timer.getFPS(), cam:toString())
	love.graphics.printf(right_hud, 0, 0, 800, 'right')

	jprof.pop("frame")
end

function love.mousemoved(_, _, dx, dy)
	--cam:mouseMoved(dx, dy)
end

function love.keypressed(k)
	if k == '.' then
		cam:toggleInput()
	elseif k == 't' then
		cam:save()
	elseif k == 'y' then
		cam:load()
	end
end

function love.threaderror(_, e)
	print(e)
end
