-- profile hooks
local jprof = require 'jprof'
local Asset = require 'gltf.asset'
local Renderer = require 'gltf.defaultRenderer'
local dkjson = require 'gltf.dkjson'
local mat4 = require 'cpml.modules.mat4'
local vec3 = require 'cpml.modules.vec3'
local quat = require 'cpml.modules.quat'
local ok, cjson = pcall(require, "cjson")
if not ok then cjson = nil end

_G.jprof = jprof
local function hookFunction(tbl, key, name)
	if not tbl then
		return
	end
	local tmp = {n=0}
	local function pack(...)
		local n = select('#', ...)
		for i = 1, n do
			tmp[i] = select(i, ...)
		end
		for i = n+1, tmp.n do
			tmp[i] = nil
		end
		tmp.n = n
		return tmp
	end

	local real = tbl[key]
	tbl[key] = function(...)
		jprof.push(name)
		local ret = pack(real(...))
		jprof.pop(name)
		return unpack(ret, 1, ret.n)
	end
end

hookFunction(love.filesystem, "newFileData", "love.filesystem.newFileData()")
hookFunction(love.data, "newByteData", "love.data.newByteData()")
hookFunction(love.graphics, "newMesh", "love.graphics.newMesh()")
hookFunction(love.graphics, "newImage", "love.graphics.newImage()")
hookFunction(love.image, "newImageData", "love.graphics.newImageData()")
hookFunction(love.image, "newCompressedData", "love.graphics.newCompressedData()")

hookFunction(dkjson, "decode", "dkjson.decode()")
hookFunction(cjson, "decode",  "cjson.decode()")

hookFunction(mat4, "new", "mat4.new()")
hookFunction(mat4, "mul", "mat4.mul()")
hookFunction(mat4, "translate", "mat4.translate()")
hookFunction(mat4, "rotate", "mat4.rotate()")
hookFunction(mat4, "scale", "mat4.scale()")
hookFunction(vec3, "new", "vec3.new()")
hookFunction(quat, "new", "quat.new()")

hookFunction(Asset, "loadGLTF", "Asset:loadGLTF()")
hookFunction(Asset, "loadAnimation", "Asset:loadAnimation()")
hookFunction(Asset, "buildTransformCacheForScene", "Asset:buildTransformCacheForScene()")
hookFunction(Asset, "loadMesh", "Asset:loadMesh()")
hookFunction(Asset, "_convertPackedData", "Asset:_convertPackedData()")
hookFunction(Asset, "loadMaterial", "Asset:loadMaterial()")
hookFunction(Asset, "loadTexture", "Asset:loadTexture()")
--hookFunction(Asset, "getDataForAccessor", "Asset:getDataForAccessor()")
--hookFunction(Asset, "getMeshIndexForNode", "Asset:getMeshIndexForNode()")
--hookFunction(Asset, "_calculateTransform", "Asset:_calculateTransform()")
--hookFunction(Asset, "_calculateMorphWeights", "Asset:_calculateMorphWeights()")

hookFunction(Asset.AnimationChannel, "buildFrames", "AnimationChannel:buildFrames()")
--hookFunction(Asset.AnimationChannel, "replace", "AnimationChannel:replace()")

local AssetInstance = Asset.AssetInstance
hookFunction(AssetInstance, "buildFrames", "AnimationChannel:buildFrames()")
hookFunction(AssetInstance, "updateTransformCache", "AssetInstance:updateTransformCache()")
hookFunction(AssetInstance, "_calculateJointMatrices", "AssetInstances:_calculateJointMatrices()")
--hookFunction(AssetInstance, "getJointMatricesForNode", "Asset:getJointMatricesForNode()")
--hookFunction(AssetInstance, "getMorphWeightsForNode", "Asset:getMorphWeightsForNode()")

hookFunction(Renderer, "addToDrawList", "Renderer:addToDrawList()")
hookFunction(Renderer, "draw", "Renderer:draw()")
