local ok, imgui = pcall(require, 'imgui')

local debugUI = {}

if true or not ok then
	function debugUI:inject() end
	function debugUI:get() return false end -- always check if this value is truthy before using
	return debugUI
end

function debugUI:get() return imgui end

function debugUI:inject()
	imgui.NewFrame()
	local _update = love.update
	function love.update(dt)
		_update(dt)
	end

	local _draw = love.draw
	function love.draw()
		_draw()
		imgui.Render()
		imgui.NewFrame()
	end

	local function empty() end

	local _quit = love.quit or empty
	function love.quit()
		imgui.ShutDown()
		return _quit()
	end

	local _textinput = love.textinput or empty
	function love.textinput(t)
		imgui.TextInput(t)
		if not imgui.GetWantCaptureKeyboard() then
			_textinput(t)
		end
	end

	local _keypressed = love.keypressed or empty
	function love.keypressed(t)
		imgui.KeyPressed(t)
		if not imgui.GetWantCaptureKeyboard() then
			_keypressed(t)
		end
	end

	local _keyreleased = love.keyreleased or empty
	function love.keyreleased(t)
		imgui.KeyReleased(t)
		if not imgui.GetWantCaptureKeyboard() then
			_keyreleased(t)
		end
	end

	local _mousemoved = love.mousemoved or empty
	function love.mousemoved(x, y)
		imgui.MouseMoved(x, y)
		if not imgui.GetWantCaptureMouse() then
			_mousemoved(x, y)
		end
	end

	local _mousepressed = love.mousepressed or empty
	function love.mousepressed(x, y, btn)
		imgui.MousePressed(btn)
		if not imgui.GetWantCaptureMouse() then
			_mousepressed(x, y, btn)
		end
	end

	local _mousereleased = love.mousereleased or empty
	function love.mousereleased(x, y, btn)
		imgui.MouseReleased(btn)
		if not imgui.GetWantCaptureMouse() then
			_mousereleased(x, y, btn)
		end
	end

	local _wheelmoved = love.wheelmoved or empty
	function love.wheelmoved(y)
		imgui.WheelMoved(y)
		if not imgui.GetWantCaptureMouse() then
			_wheelmoved(y)
		end
	end
end

return debugUI
