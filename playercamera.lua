local vec3 = require 'cpml.modules.vec3'
local mat4 = require 'cpml.modules.mat4'
local json = require 'gltf.dkjson'

local PlayerCamera = {}
local PlayerCamera_mt = { __index = PlayerCamera }

local upAxis = vec3.new(0, 1, 0)
local forwardAxis = vec3.new(0, 0, 1)
local maxAngle = 89
function PlayerCamera.new()
	local self = setmetatable({}, PlayerCamera_mt)
	self.pos = vec3.new()
	self.angleX = 0
	self.angleY = 0
	self.direction = vec3.new()
	self.transform = mat4.new()
	return self
end

function PlayerCamera:update(dt)
	local vright, vup, vforward = 0, 0, 0
	local vlookup, vlookright = 0, 0
	if love.keyboard.isDown'w' then
		vforward = vforward + 1
	end
	if love.keyboard.isDown's' then
		vforward = vforward - 1
	end

	if love.keyboard.isDown'a' then
		vright = vright - 1
	end
	if love.keyboard.isDown'd' then
		vright = vright + 1
	end

	if love.keyboard.isDown'f' then
		vup = vup - 1
	end
	if love.keyboard.isDown'r' then
		vup = vup + 1
	end

	if love.keyboard.isDown'left' then
		vlookright = vlookright - 1
	end
	if love.keyboard.isDown'right' then
		vlookright = vlookright + 1
	end

	if love.keyboard.isDown'down' then
		vlookup = vlookup - 1
	end
	if love.keyboard.isDown'up' then
		vlookup = vlookup + 1
	end
	self.angleX = self.angleX + (vlookright * 180 * dt)
	self.angleY = math.min(math.max(self.angleY + vlookup * 180 * dt, -maxAngle), maxAngle)

	local ax, ay = math.rad(self.angleX), math.rad(self.angleY)
	self.direction = vec3.new(1, 0, 0)
		:rotate(ay, forwardAxis)
		:rotate(-ax, upAxis)

	local speedParallel = 100
	local speedPerpendicular = 100
	local speedUpDown = 50
	local scale = vforward*speedParallel*dt
	local d = vec3.new(self.direction.x*scale, self.direction.y*scale, self.direction.z*scale)
	self.pos = self.pos:add(d)
	d = self.direction:cross(upAxis) * (vright*speedPerpendicular*dt)
	self.pos = self.pos:add(d)
	self.pos.y = self.pos.y + vup*speedUpDown*dt
end

function PlayerCamera:mouseMoved(dx, dy)
	if love.mouse.getRelativeMode() then
		self.angleX = self.angleX + (dx * .5)
		self.angleY = math.min(math.max(self.angleY + dy * .5, -maxAngle), maxAngle)
	end
end

function PlayerCamera:getTransform()
	self.transform:look_at(self.transform, self.pos, self.pos+self.direction, upAxis)
	self.transform:translate(self.transform, -self.pos)
	return self.transform
end

function PlayerCamera:grabInput()
	love.mouse.setRelativeMode(true)
end

function PlayerCamera:stopInput()
	love.mouse.setRelativeMode(false)
end

function PlayerCamera:toggleInput()
	love.mouse.setRelativeMode(not love.mouse.getRelativeMode())
end

function PlayerCamera:toString()
	return string.format(
		"position: (%.2f, %.2f, %.2f)\n direction(%.2f, %.2f, %.2f)\nangle: <%f, %f>",
		self.pos.x,
		self.pos.y,
		self.pos.z,
		self.direction.x,
		self.direction.y,
		self.direction.z,
		self.angleX,
		self.angleY
	)
end

function PlayerCamera:save()
	local data = {
		self.pos.x,
		self.pos.y,
		self.pos.z,
		self.direction.x,
		self.direction.y,
		self.direction.z,
		self.angleX,
		self.angleY
	}
	love.filesystem.write("camera.save", json.encode(data))
end

function PlayerCamera:load()
	local datastr = love.filesystem.read("camera.save")
	if datastr then
		local data, a, b = json.decode(datastr)
		self.pos.x = data[1]
		self.pos.y = data[2]
		self.pos.z = data[3]
		self.direction.x = data[4]
		self.direction.y = data[5]
		self.direction.z = data[6]
		self.angleX = data[7]
		self.angleY = data[8]
	end
end

return PlayerCamera
