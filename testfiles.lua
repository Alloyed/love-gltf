return {
	["Triangle"] = {
		["loose"] = "glTF-Sample-Models/2.0/Triangle/glTF/Triangle.gltf",
		["embedded"] = "glTF-Sample-Models/2.0/Triangle/glTF-Embedded/Triangle.gltf",
	},
	["Cameras"] = {
		["loose"] = "glTF-Sample-Models/2.0/Cameras/glTF/Cameras.gltf",
		["embedded"] = "glTF-Sample-Models/2.0/Cameras/glTF-Embedded/Cameras.gltf",
	},
	["BoxTextured"] = {
		["loose"] = "glTF-Sample-Models/2.0/BoxTextured/glTF/BoxTextured.gltf",
		["embedded"] = "glTF-Sample-Models/2.0/BoxTextured/glTF-Embedded/BoxTextured.gltf",
		["binary"] = "glTF-Sample-Models/2.0/BoxTextured/glTF-Binary/BoxTextured.glb",
	},
	["BoxInterleaved"] = {
		["loose"] = "glTF-Sample-Models/2.0/BoxInterleaved/glTF/BoxInterleaved.gltf",
		["embedded"] = "glTF-Sample-Models/2.0/BoxInterleaved/glTF-Embedded/BoxInterleaved.gltf",
		["binary"] = "glTF-Sample-Models/2.0/BoxInterleaved/glTF-Binary/BoxInterleaved.glb",
	},
	["BoxVertexColors"] = {
		["loose"] = "glTF-Sample-Models/2.0/BoxVertexColors/glTF/BoxVertexColors.gltf",
		["embedded"] = "glTF-Sample-Models/2.0/BoxVertexColors/glTF-Embedded/BoxVertexColors.gltf",
	},
	["Duck"] = {
		["loose"] = "glTF-Sample-Models/2.0/Duck/glTF/Duck.gltf",
		["embedded"] = "glTF-Sample-Models/2.0/Duck/glTF-Embedded/Duck.gltf",
	},
	["BoomBoxWithAxes"] = {
		["loose"] = "glTF-Sample-Models/2.0/BoomBoxWithAxes/glTF/BoomBoxWithAxes.gltf",
	},
	["VertexColorTest"] = {
		["loose"] = "glTF-Sample-Models/2.0/VertexColorTest/glTF/VertexColorTest.gltf",
		["embedded"] = "glTF-Sample-Models/2.0/VertexColorTest/glTF-Embedded/VertexColorTest.gltf",
	},
	["TextureSettingsTest"] = {
		["loose"] = "glTF-Sample-Models/2.0/TextureSettingsTest/glTF/TextureSettingsTest.gltf",
		["embedded"] = "glTF-Sample-Models/2.0/TextureSettingsTest/glTF-Embedded/TextureSettingsTest.gltf",
	},
	["OrientationTest"] = {
		["loose"] = "glTF-Sample-Models/2.0/OrientationTest/glTF/OrientationTest.gltf",
		["embedded"] = "glTF-Sample-Models/2.0/OrientationTest/glTF-Embedded/OrientationTest.gltf",
	},
	["Buggy"] = {
		["loose"] = "glTF-Sample-Models/2.0/Buggy/glTF/Buggy.gltf",
	},
	["2CylinderEngine"] = {
		["loose"] = "glTF-Sample-Models/2.0/2CylinderEngine/glTF/2CylinderEngine.gltf",
	},
	["TextureCoordinateTest"] = {
		["loose"] = "glTF-Sample-Models/2.0/TextureCoordinateTest/glTF/TextureCoordinateTest.gltf",
	},
	["Avocado"] = {
		["loose"] = "glTF-Sample-Models/2.0/Avocado/glTF/Avocado.gltf",
	},
	["Sponza"] = {
		["loose"] = "glTF-Sample-Models/2.0/Sponza/glTF/Sponza.gltf",
	},
	["VC"] = {
		["loose"] = "glTF-Sample-Models/2.0/VC/glTF/VC.gltf",
	},
	["SciFiHelmet"] = {
		["loose"] = "glTF-Sample-Models/2.0/SciFiHelmet/glTF/SciFiHelmet.gltf",
	},
	["InterpolationTest"] = {
		["loose"] = "glTF-Sample-Models/2.0/InterpolationTest/glTF/InterpolationTest.gltf",
	},
	["AnimatedCube"] = {
		["loose"] = "glTF-Sample-Models/2.0/AnimatedCube/glTF/AnimatedCube.gltf",
	},
	["BoxAnimated"] = {
		["loose"] = "glTF-Sample-Models/2.0/BoxAnimated/glTF/BoxAnimated.gltf",
	},
	["Monster"] = {
		["loose"] = "glTF-Sample-Models/2.0/Monster/glTF/Monster.gltf",
	},
	["CesiumMan"] = {
		["loose"] = "glTF-Sample-Models/2.0/CesiumMan/glTF/CesiumMan.gltf",
	},
	["CesiumMilkTruck"] = {
		["loose"] = "glTF-Sample-Models/2.0/CesiumMilkTruck/glTF/CesiumMilkTruck.gltf",
	},
	["RiggedSimple"] = {
		["loose"] = "glTF-Sample-Models/2.0/RiggedSimple/glTF/RiggedSimple.gltf",
	},
	["AnimatedTriangle"] = {
		["loose"] = "glTF-Sample-Models/2.0/AnimatedTriangle/glTF/AnimatedTriangle.gltf",
	},
	["AlphaBlendModeTest"] = {
		["loose"] = "glTF-Sample-Models/2.0/AlphaBlendModeTest/glTF/AlphaBlendModeTest.gltf",
	},
	["SimpleMorph"] = {
		["loose"] = "glTF-Sample-Models/2.0/SimpleMorph/glTF/SimpleMorph.gltf",
	},
	["SimpleSparseAccessor"] = {
		["loose"] = "glTF-Sample-Models/2.0/SimpleSparseAccessor/glTF/SimpleSparseAccessor.gltf",
	},
	["AnimatedMorphCube"] = {
		["loose"] = "glTF-Sample-Models/2.0/AnimatedMorphCube/glTF/AnimatedMorphCube.gltf",
	},
	["AnimatedMorphSphere"] = {
		["loose"] = "glTF-Sample-Models/2.0/AnimatedMorphSphere/glTF/AnimatedMorphSphere.gltf",
	},
	["UnlitTest"] = {
		["loose"] = "glTF-Sample-Models/2.0/UnlitTest/glTF/UnlitTest.gltf",
	},
	["MorphPrimitivesTest"] = {
		["loose"] = "glTF-Sample-Models/2.0/MorphPrimitivesTest/glTF/MorphPrimitivesTest.gltf",
	},
	["MultiUVTest"] = {
		["loose"] = "glTF-Sample-Models/2.0/MultiUVTest/glTF/MultiUVTest.gltf",
	},
	["LittlestTokyo"] = {
		["loose"] = "sketchfab-samples/littlest_tokyo/scene.gltf",
	},
	["kgirl1"] = {
		["loose"] = "sketchfab-samples/kgirl1/scene.gltf",
	},
}
