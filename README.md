# love-gltf
A partial loader for [glTF](https://github.com/KhronosGroup/glTF) 2.0 assets.

To install, copy the gltf folder into your project. you should also have a copy
of cpml.

All public methods are in the [API Documentation](docs/gltf.md).
Everything else should be considered private and subject to wild changes at any
time.

MIT license, (c) kyle mclamb.

## Viewer
Everything outside the gltf folder is part of the reference viewer, which can
be used to test various models

Usage:
```
	love . <asset-path>
```
to view an asset

```
	love . --builtin <asset-name>
```
to view a built-in asset

## Performance tips
* When available, love-gltf will use cjson to parse the initial json
  structure, which provides a significant speedup. lpeg will also provide a speedup, albeit a smaller one.
* using compressed dds textures will also improve performance compared to using jpeg/png

## Supported Extensions
* [MSFT_texture_dds](https://github.com/KhronosGroup/glTF/tree/master/extensions/2.0/Vendor/MSFT_texture_dds)

## TODO
Implemented:
* meshes
* basic unlit materials
* Scene graph
* animations
* Public API for materials/animations
* skins
* morph targets

Planned:
- [ ] A way to write custom shaders without having to reinvent everything in the default shader
- [ ] async/incremental loading
- [ ] KHR_texture_transform
- [ ] MSFT_lod
- [ ] EXT_property_animation (only some of them)
- [ ] procedurally created meshes/properties/animations

I have these listed as out of scope, either because the effort/usefulness ratio
seems low or just because I don't see the value in them. If you disagree, feel
free to open an issue :)
Out-of-scope:
* PBR reference renderer
* Cameras
* CUBICSPLINE animations
* Line meshes
* load from URL
