local dkjson = require 'gltf.dkjson'
local args = {...}

io.stderr:write("opening ", args[1], "\n")
local f = assert(io.open(args[1], 'r'))
local s = f:read('*a')
f:close()

local json, _, err = dkjson.decode(s)
if not json then error(err) end
for _, textureJson in ipairs(json.textures) do
	if textureJson.extensions == nil or textureJson.extensions["MSFT_texture_dds"] == nil then
		-- time to dds-ify
		local imageJson = json.images[textureJson.source + 1]
		if imageJson.uri then
			local newUri = imageJson.uri:gsub("%.png$", ".dds"):gsub("%.jpg", ".dds"):gsub("%.jpeg", ".dds")
			if newUri ~= imageJson.uri then
				local newImage = {}
				for k, v in pairs(imageJson) do
					newImage[k] = v
				end
				newImage.uri = newUri
				local sourceIdx = #json.images + 1
				table.insert(json.images, newImage)
				textureJson.extensions = textureJson.extensions or {}
				textureJson.extensions["MSFT_texture_dds"] = {
					source = sourceIdx - 1
				}
			end
		end
	end
end

local out_s = assert(dkjson.encode(json))
io.write(out_s) -- To save do a pipe in your shell :)
