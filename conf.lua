if love.filesystem then
	local ok, loader = pcall(require, 'rocks')
	if ok then loader() end
end

function love.conf(t)
	t.window.resizable = true
	--t.window.vsync = 0
	t.identity = "gltfviewer"
	t.rocks_servers = {
		"http://alloyed.me/shared/rocks",
		"http://luarocks.org/dev",
	}
	t.dependencies = {
		"lua-cjson ~> 2.1",
		"love-imgui ~> 0.9",
		"cpml ~> scm",
	}
end
