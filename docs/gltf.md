# Modules/Classes 

* \[class\] [Asset](#asset)
* \[class\] [GLTFMaterial](#gltfmaterial)
* \[class\] [GLTFPrimitive](#gltfprimitive)
* \[class\] [GLTFMesh](#gltfmesh)
* \[class\] [Context](#context)
* \[module\] [gltf](#gltf)
# Asset

Each asset represents one .gltf/.glb file. This can, in turn, hold any
number of meshes, animations, scenes, or textures. It'll be up to your
application to decide what it wants to use from each asset and when.

## Asset:getNumTextures()<a id="assetgetnumtextures"></a>

returns the number of textures in the asset.

**Return Value**: numTextures

## Asset:loadTexture(textureIdx)<a id="assetloadtexture"></a>

loads a love texture, including eagerly loading the underlying image
and generating mipmaps if necessary

**Parameters**:
- *textureIdx*

**Return Value**: texture

**See also**: [Texture](https://love2d.org/wiki/Texture)
## Asset:loadMaterialByName(materialName)<a id="assetloadmaterialbyname"></a>

**Parameters**:
- *materialName*

**Return Value**: material

**See also**: [GLTFMaterial](#gltfmaterial)
## Asset:getNumMaterials()<a id="assetgetnummaterials"></a>

returns the number of materials in the asset

**Return Value**: numMaterials

## Asset:loadDefaultMaterial()<a id="assetloaddefaultmaterial"></a>

when a mesh primitive doesn't specify a material, this material is
used instead. It is a simple, untextured, grey color.

**Return Value**: material

**See also**: [GLTFMaterial](#gltfmaterial)
## Asset:loadMaterial(materialIdx)<a id="assetloadmaterial"></a>

loads a gltf material.

**Parameters**:
- *materialIdx*

**Return Value**: material

**See also**: [GLTFMaterial](#gltfmaterial)
## Asset:loadMeshByName(meshName)<a id="assetloadmeshbyname"></a>

**Parameters**:
- *meshName*: the string name of the mesh

**Return Value**: mesh

**See also**: [GLTFMesh](#gltfmesh)
## Asset:loadAllMeshes()<a id="assetloadallmeshes"></a>

Eagerly loads all meshes in advance. Every referenced material/texture
will also be loaded.

## Asset:getNumMeshes()<a id="assetgetnummeshes"></a>

Returns the number of meshes in the asset. Note that there's no
requirement that every mesh be used by every scene, or that meshes get used
at all.

**Return Value**: numMeshes

## Asset:loadMesh(meshIdx)<a id="assetloadmesh"></a>

**Parameters**:
- *meshIdx*: the mesh index

**Return Value**: mesh

**See also**: [GLTFMesh](#gltfmesh)
## Asset:getNumScenes()<a id="assetgetnumscenes"></a>

Returns the number of scenes in the asset

**Return Value**: numScenes

## Asset:iterateSceneMeshNodes()<a id="assetiteratescenemeshnodes"></a>

iterates over the index of every scene node that has a mesh
associated. This can be used to draw every mesh in a scene

**Return Value**: iterator, data, initalIndex

## Asset:loadAnimations()<a id="assetloadanimations"></a>

loads all animations. Unlike other types, each animation _must_ be
loaded eagerly, and will do that construction time by default.

## Asset:buildTransformCacheForScene(sceneIdx)<a id="assetbuildtransformcacheforscene"></a>

visits every node in the provided scene and calculates what its current
model transformation should be. This must be called before using
Asset:getTransformForNode, and (right now) happens eagerly at construction
time.

**Parameters**:
- *sceneIdx*: the scene index

## Asset:updateTransformCacheForScene(sceneIdx)<a id="assetupdatetransformcacheforscene"></a>

recalculates the transforms for every animated node in the scene.
Should be called at least once after animation state updates to correctly
reflect that.

**Parameters**:
- *sceneIdx*: the scene index

## Asset:loadScenes()<a id="assetloadscenes"></a>

loads metadata and builds the transform cache for every node in every
scene. Called eagerly at construction time

## Asset:updateGlobalTime(dt)<a id="assetupdateglobaltime"></a>

updates the time that each animation in the asset respects. WIP, the
current animation API is not yet stable.

**Parameters**:
- *dt*

## Asset:getTransformForNode(nodeIdx)<a id="assetgettransformfornode"></a>

returns the cached model transformation for the given scene node. The
returned transform will _only_ be valid if the transform cache has been
built and is up to date for at least one scene this node is referenced in.

**Parameters**:
- *nodeIdx*: the node index

**Return Value**: mat4

**See also**: [Asset:buildTransformCacheForScene](#assetbuildtransformcacheforscene), [Asset:updateTransformCacheForScene](#assetupdatetransformcacheforscene)
## Asset:doesNodeHaveSkin(hasSkin)<a id="assetdoesnodehaveskin"></a>

WIP, skinning does not yet work

**Parameters**:
- *hasSkin*

## Asset:calculateJointMatricesForNode(nodeIdx)<a id="assetcalculatejointmatricesfornode"></a>

WIP, skinning does not yet work

**Parameters**:
- *nodeIdx*: the node index

# GLTFMaterial

Materials hold the color/texture/etc of a given mesh primitive.
Materials can be reused across primitives. API calls tbd

# GLTFPrimitive

Each GLTF mesh is made up of multiple primitives. A primitive is
closer to a `love.graphics` Mesh, in that it has attribute data and a
material and can be drawn in a single draw call.

## GLTFPrimitive:draw()<a id="gltfprimitivedraw"></a>

submits a draw call for the given primitive. This will destructively
edit these global `love.graphics` parameters:
* `love.graphics.getColor()`

Handling shader uniforms and model transformation is responsibility of the caller.

## GLTFPrimitive:isBlended()<a id="gltfprimitiveisblended"></a>

returns whether or not the given primitive uses the BLEND alpha mode.
This means that its material is semi-translucent and will probably need
special support to render correctly.

**Return Value**: isBlended

## GLTFPrimitive:getAlphaCutoff()<a id="gltfprimitivegetalphacutoff"></a>

returns a cutoff value, 0-1. Texels with an alpha value below the
cutoff should be discarded in the fragment shader when drawing this primitive.

**Return Value**: cutoff

# GLTFMesh

A GLTF mesh is a simple composite object: it is made up of primitives,
and each primitive is the thing that will actually submit a
`love.graphics.draw()` call etc.

## GLTFMesh:draw()<a id="gltfmeshdraw"></a>

submits a draw call for every primitive in the mesh.
This will destructively edit these global love.graphics parameters:
* `love.graphics.getColor()`

Handling shader uniforms and model transformation is responsibility of the caller.

## GLTFMesh:getPrimitives()<a id="gltfmeshgetprimitives"></a>

Returns a list of GLTFPrimitive objects

**Return Value**: primitives

# Context

This is a reference rendering path for GLTF assets.

# gltf

This is glTF 2.0 asset importer.
Each asset is contained in a gltf.Asset object. These can be used totally on
their own, but gltf.Context provides a reference rendering path you can use
to draw them, as well.

## gltf.newAsset(filename)<a id="gltfnewasset"></a>

loads a new gltf asset from file. uses love.filesystem.

**Parameters**:
- *filename*: the name of the .gltf file or .glb file

**Return Value**: asset

**See also**: [Asset](#asset)
## gltf.addCustomAttribute(dataName, shaderName)<a id="gltfaddcustomattribute"></a>

defines a custom attribute that meshes can support.

**Parameters**:
- *dataName*: the name of the attribute in the gltf source (eg. TEXCOORD_0)
- *shaderName*: the name the attribute should be bound to in glsl (eg. VertexTexCoord)

## gltf.newContext()<a id="gltfnewcontext"></a>

Creates a new gltf rendering context

**Return Value**: context

**See also**: [Context](#context)
